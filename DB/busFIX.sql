

INSERT INTO USER_INFO (FullName,IdentifyCard,Gender,BirthDay,PhoneNumber,Address,Email)
VALUES ('Admin System','192118222',1,Convert(datetime, '30/07/1997', 103),'0123454355','Di An,SG','adminsystem@gmail.com');

INSERT INTO USER_INFO (FullName,IdentifyCard,Gender,BirthDay,PhoneNumber,Address,Email)
VALUES ('Seller System','192118333',1,Convert(datetime, '31/10/1996', 103),'012345433','Di An,SG','sellersystem@gmail.com');

INSERT INTO USER_INFO (FullName,IdentifyCard,Gender,BirthDay,PhoneNumber,Address,Email)
VALUES ('Customer System','192118444',1,Convert(datetime, '20/02/1998', 103),'0123453355','Di An,SG','customersystem@gmail.com');

INSERT INTO USER_INFO (FullName,IdentifyCard,Gender,BirthDay,PhoneNumber,Address,Email)
VALUES ('Minh Anh','1921653444',0,Convert(datetime, '20/12/1995', 103),'0123453366','Thu Duc,SG','minhchau@gmail.com');

INSERT INTO USER_INFO (FullName,IdentifyCard,Gender,BirthDay,PhoneNumber,Address,Email)
VALUES ('Cuong Hao','192777444',0,Convert(datetime, '31/01/1997', 103),'0123453455','Long An','cuscuong@gmail.com');

SELECT * FROM USER_INFO

SELECT * FROM ACCOUNT

SELECT * FROM TRIP



alter table TRIP
add constraint df_TRIP_Counts default 0 for Counts

update TRIP set Counts = 0 

SELECT * FROM SCHEDULE


alter table SCHEDULE
add BusId  int unique  references BUS(BusId)  

alter table STICKETS
add BusId int references BUS(BusId) 

update SCHEDULE set BusId = null

INSERT INTO ACCOUNT (Username,Password,Role,UserId)
VALUES ('admin','admin','admin',3) 

INSERT INTO ACCOUNT (Username,Password,Role,UserId)
VALUES ('seller','seller','seller',4) 

INSERT INTO ACCOUNT (Username,Password,Role,UserId)
VALUES ('customer','customer','customer',5) 

INSERT INTO ACCOUNT (Username,Password,Role,UserId)
VALUES ('phong','123456','customer',2) 

INSERT INTO ACCOUNT (Username,Password,Role,UserId)
VALUES ('minhanh','123456','customer',6) 

INSERT INTO ACCOUNT (Username,Password,Role,UserId)
VALUES ('hoanghoang','123456','customer',7) 

INSERT INTO TRIP (TripName,TripLenght,BusType,UserId,Cost)
VALUES ('SAI GON - NHA TRANG',430,'bus-30',4,300)

INSERT INTO TRIP (TripName,TripLenght,BusType,UserId,Cost)
VALUES ('NHA TRANG-SAI GON',430,'bus-30',4,350)

INSERT INTO TRIP (TripName,TripLenght,BusType,UserId,Cost)
VALUES ('SAI GON - BINH DINH',635,'bus-30',4,400)

INSERT INTO TRIP (TripName,TripLenght,BusType,UserId,Cost)
VALUES ('BINH DINH - SAI GON',635,'bus-30',4,450)

INSERT INTO TRIP (TripName,TripLenght,BusType,UserId,Cost)
VALUES ('BINH DINH - HA NOI',635,'bus-30',4,450)


-- schedule--
SELECT * FROM SCHEDULE

INSERT INTO SCHEDULE(StartTime, EndTime, TripId)
VALUES (Convert(time, '18:00', 103), Convert(time, '04:00', 103), 1)

INSERT INTO SCHEDULE(StartTime, EndTime, TripId)
VALUES (Convert(time, '06:00', 103), Convert(time, '16:00', 103), 1)

INSERT INTO SCHEDULE(StartTime, EndTime, TripId)
VALUES (Convert(time, '18:00', 103), Convert(time, '04:00', 103), 2)

INSERT INTO SCHEDULE(StartTime, EndTime, TripId)
VALUES (Convert(time, '06:00', 103), Convert(time, '16:00', 103), 2)

INSERT INTO SCHEDULE(StartTime, EndTime, TripId)
VALUES (Convert(time, '18:00', 103), Convert(time, '06:00', 103), 3)

INSERT INTO SCHEDULE(StartTime, EndTime, TripId)
VALUES (Convert(time, '06:00', 103), Convert(time, '18:00', 103), 3)

INSERT INTO SCHEDULE(StartTime, EndTime, TripId)
VALUES (Convert(time, '18:00', 103), Convert(time, '06:00', 103), 4)

INSERT INTO SCHEDULE(StartTime, EndTime, TripId)
VALUES (Convert(time, '06:00', 103), Convert(time, '18:00', 103), 4)

-------BUS-----
SELECT * FROM  BUS

INSERT INTO BUS (BusName,BusNo,SlotTotal)
VALUES ('Phuong Trang 1','95X1-56784',30)

INSERT INTO BUS (BusName,BusNo,SlotTotal)
VALUES ('Phuong Trang 2','95X1-53384',30)

INSERT INTO BUS (BusName,BusNo,SlotTotal)
VALUES ('Phuong Trang 3','95X1-57784',15)

INSERT INTO BUS (BusName,BusNo,SlotTotal)
VALUES ('Hoang Long 1','95C2-59086',30)
INSERT INTO BUS (BusName,BusNo,SlotTotal)
VALUES ('Hoang Long 2','95C2-59099',30)
INSERT INTO BUS (BusName,BusNo,SlotTotal)
VALUES ('Hoang Long 4','95C2-59999',30)
INSERT INTO BUS (BusName,BusNo,SlotTotal)
VALUES ('Hoang Long 5','95C2-59888',30)
INSERT INTO BUS (BusName,BusNo,SlotTotal)
VALUES ('Hoang Long 3','95C2-59123',15)
------------STICKETS-------------
SELECT * FROM STICKETS
INSERT INTO STICKETS (SticketId,SeatNo)
VALUES (1,1)

DECLARE @Number INT = 151 ;
WHILE @Number < =   210
BEGIN
	INSERT INTO STICKETS (SticketId,SeatNo) VALUES (@Number,@Number);
	SET @Number = @Number + 1 ;
END
------STICKET_OF_BUS----------
SELECT * FROM STICKET_OF_BUS

INSERT INTO STICKET_OF_BUS(BusId,ScheduleId,SticketId) VALUES (1,1,1)

DECLARE @Number INT = 181;
WHILE @Number < =   210
BEGIN
	INSERT INTO STICKET_OF_BUS(BusId,ScheduleId,SticketId) VALUES (7,8,@Number);
	SET @Number = @Number + 1 ;
END

--------------------------DEPARTURE_TIME-------------
SELECT * FROM DEPARTURE_TIME
update DEPARTURE_TIME set DepartureDate=Convert(datetime, '01/08/2018 18:00', 103) where TimeId=2

INSERT INTO DEPARTURE_TIME (DepartureDate) VALUES (Convert(datetime, '01/08/2018 06:00', 103))
INSERT INTO DEPARTURE_TIME (DepartureDate) VALUES (Convert(datetime, '01/08/2018 18:00', 103))

INSERT INTO DEPARTURE_TIME (DepartureDate) VALUES (Convert(datetime, '03/08/2018 06:00', 103))
INSERT INTO DEPARTURE_TIME (DepartureDate) VALUES (Convert(datetime, '03/08/2018 18:00', 103))

-----------------------BOOKING--------------
SELECT * FROM BOOKING
SELECT * FROM STICKET_STATUS

CREATE TRIGGER bookingStatus ON BOOKING
FOR INSERT
AS
BEGIN
	DECLARE @TimeId int;
	DECLARE @UserId int;
	DECLARE @SticketId int;
	SELECT @TimeId=i.TimeId from inserted i;	
	SELECT @SticketId=i.SticketId from inserted i;	
	INSERT INTO STICKET_STATUS(Booked,SticketId,TimeId) VALUES(1,@SticketId,@TimeId)
END

INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (1,2,1)
INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (2,2,1)
INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (6,5,1)
INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (7,5,1)
INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (7,6,1)
INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (7,7,1)

INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (6,8,3)
INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (7,5,4)
INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (7,8,3)
INSERT INTO BOOKING (SticketId,UserId,TimeId) VALUES (7,7,4)
------------------------------------------------------
UPDATE  STICKET_OF_BUS SET BusId=8 WHERE ScheduleId=7;
DELETE SCHEDULE WHERE TripId=1
DELETE USER_INFO WHERE UserId>=6
UPDATE TRIP SET TripName='NT-SG' WHERE TripName='NHA TRANG-SAI GON'
UPDATE TRIP SET TripName='BD-SG' WHERE TripName='BINH DINH - SAI GON'
UPDATE TRIP SET TripName='SG-BD' WHERE TripName='SAI GON - BINH DINH'
--SELECT * from USER_INFO ORDER BY UserId DESC LIMIT 1
SELECT TOP 1 * FROM USER_INFO ORDER BY UserId DESC 
-----------------------------Customer-BOOKING-----------------------
select * from ACCOUNT

select * FROM SCHEDULE x INNER JOIN TRIP y ON x.TripId=y.TripId WHERE TripName = 'SG-NT'

select * from DEPARTURE_TIME a inner join BOOKING b on a.TimeId=b.TimeId

select BusId,ScheduleId,x.SticketId,UserId,z.DepartureDate from (STICKET_OF_BUS x inner join BOOKING y on x.SticketId = y.SticketId) inner join DEPARTURE_TIME z on z.TimeId=y.TimeId

SELECT convert(time(0),DepartureDate) from DEPARTURE_TIME

select * FROM (SCHEDULE x INNER JOIN STICKET_OF_BUS y ON x.ScheduleId=y.ScheduleId) WHERE x.ScheduleId=2 and not exists (
select * from DEPARTURE_TIME a inner join BOOKING b on a.TimeId=b.TimeId where y.SticketId = b.SticketId )

