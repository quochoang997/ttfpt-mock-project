/*
*Training Fresher Academy
*author Oracle-PC
*date 25 thg 7, 2018
*/
package bus.sticket.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import bus.sticket.model.Account;
import bus.sticket.model.UserInfo;
import bus.sticket.service.UserServiceImpl;

@Controller
public class LoginController {
	@Autowired
	private UserServiceImpl serviceImpl;

	@RequestMapping(value = "login")
	public String showLogin(Model model) {
		model.addAttribute("userInfo", new UserInfo());
		return "login";
	}

	@RequestMapping(value = "dashboard")
	public String login(@ModelAttribute("userInfo") UserInfo userInfo, Model model,HttpSession session) {
		Account accountReturn = serviceImpl.login(userInfo);
		if (accountReturn != null) {
			session.setAttribute("currentUser",accountReturn);
			if (accountReturn.isAdmin()) {
				model.addAttribute("userInfo", new UserInfo());
				return "admin";
			} else if (accountReturn.isSeller()) {
				model.addAttribute("userInfo", new UserInfo());
				return "seller";
			} else if (accountReturn.isCustomer()) {
				model.addAttribute("userInfo", new UserInfo());
				return "customer";
			}
		}
		model.addAttribute("error", true);
		return "login";
	}
}
