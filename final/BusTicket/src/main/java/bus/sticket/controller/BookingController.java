/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import bus.sticket.service.TripService;

@Controller
public class BookingController {
	
	@Autowired
	private TripService tripService;
	
	public TripService getTripService() {
		return tripService;
	}
	
	
	
	@RequestMapping(value="booking_action")
	public String viewTripStartTime(@RequestParam String scheduleId, Model model) {
		
		
		return "trip_start_time_view";
	}
}
