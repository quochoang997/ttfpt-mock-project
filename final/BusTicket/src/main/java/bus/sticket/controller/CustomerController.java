/*
z*Training Fresher Academy
*author Oracle-PC
*date 26 thg 7, 2018
*/
package bus.sticket.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import bus.sticket.dao.UserImplDAO;
import bus.sticket.model.Account;
import bus.sticket.model.UserInfo;
import bus.sticket.service.UserServiceImpl;

@Controller
public class CustomerController {
	@Autowired
	private UserImplDAO userImplDAO;
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	
	@RequestMapping(value="showUpdatePage")
	public String showInfoUser(Model model,HttpSession session) {
		Account accountReturn = (Account)session.getAttribute("currentUser");
		model.addAttribute("userInfo",userImplDAO.getUser(accountReturn.getUserId()));
		return "changeInfo";
	}
	@RequestMapping(value="updateInfo")
	public String customerUpdateInfo(@ModelAttribute("userInfo") UserInfo userInfo,Model model) {
		System.out.println("Con chim non");
		userServiceImpl.updateInfo(userInfo);
		return "customer";
	}
}