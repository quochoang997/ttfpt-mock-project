/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import bus.sticket.model.Schedule;
import bus.sticket.model.Trip;
import bus.sticket.service.TripService;

@Controller
public class TripController {
	
	@Autowired
	private TripService tripService;
	
	public TripService getTripService() {
		return tripService;
	}

	public void setTripService(TripService tripService) {
		this.tripService = tripService;
	}

	public TripController() {
		super();
	}

	public TripController(TripService tripService) {
		super();
		this.tripService = tripService;
	}

	@RequestMapping(value="view_trip_action")
	public String viewTripAction(Model model) {
		List<Trip> list = tripService.getAllTrip();
		model.addAttribute("listTrip", list);
		return "trip_view";
	}
	
	@RequestMapping(value="view_trip")
	public String viewTrip(Model model) {
		model.addAttribute(new Trip());
		return "trip_view";
	}
	
	@RequestMapping(value="view_trip_start_time")
	public String viewTripStartTime(@RequestParam(value="tripId") String tripId, Model model) {
		Trip trip = tripService.getTrip(Integer.parseInt(tripId));
		
		// get list Schedule
		List<Schedule> listSchedule = tripService.getListSchedulebyTripId(trip);
		model.addAttribute("trip", trip);
		model.addAttribute("listSchedule", listSchedule);
		
		return "trip_start_time_view";
	}
}
