package bus.sticket.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import bus.sticket.model.Account;
import bus.sticket.model.UserInfo;
import bus.sticket.service.UserServiceImpl;

@Controller
public class RegisterController {
	@Autowired
	private UserServiceImpl serviceImpl;
	
	@RequestMapping(value="register")
	public String showRegister(Model model) {
		model.addAttribute("userInfo", new UserInfo());
		return "register";
	}
	@RequestMapping(value="userRegister")
	public String register(@ModelAttribute("userInfo") UserInfo userInfo, Model model,HttpSession session) {
		Account accountReturn = (Account)session.getAttribute("currentUser");
		
		//----------admin-create user------------------
		if(accountReturn.isAdmin()) {
			if(userInfo.isNull()) {
				model.addAttribute("error",true);
				return "adminCreate";
			}
			if(serviceImpl.register(userInfo)) {
				return "admin";
			}
			else {
				model.addAttribute("error",true);
				return "adminCreate";
			}
		}
		
		//----------User register------------------
		if(userInfo.isNull()) {
			model.addAttribute("error",true);
			return "register";
		}
		if(serviceImpl.register(userInfo)) {
			System.out.println("Register success !!!!");
			return "login";
		}
		else {
			model.addAttribute("error",true);
			return "register";
		}
	}
}
