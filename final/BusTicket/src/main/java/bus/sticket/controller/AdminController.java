/*
*Training Fresher Academy
*author Oracle-PC
*date 30 thg 7, 2018
*/
package bus.sticket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import bus.sticket.dao.UserImplDAO;
import bus.sticket.model.UserInfo;
import bus.sticket.service.UserServiceImpl;

@Controller
public class AdminController {
	@Autowired
	private UserServiceImpl userServiceImpl;
	
	@Autowired
	private UserImplDAO userImplDAO;
	
	@RequestMapping(value="adCreateUser")
	public String adminCreateUser(Model model) {
		model.addAttribute("userInfo", new UserInfo());
		return "adminCreate";
	}
	
	@RequestMapping(value="viewAllUser")
	public String viewAllUser(Model model) {
		List<UserInfo> listUser = userImplDAO.getAllUser();
		model.addAttribute("listUser", listUser);
		return "viewAllUser";
	}
	@RequestMapping(value="deocogi")
	public String adminUpdateInfo(@RequestParam(value="userInfoID") int userId,Model model) {
		model.addAttribute("userInfo",userImplDAO.getUser(userId));
		model.addAttribute("isAdmin",true);
		return "changeInfo";
	}
}
