/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DERPARTURE_TIME")
public class DepartureTime {
	@Column(name="TimeId")
	@Id
	private int timeId;
	
	@Column(name="DepartureDate")
	private Date departureDate;

	public DepartureTime(int timeId, Date departureDate) {
		super();
		this.timeId = timeId;
		this.departureDate = departureDate;
	}

	public DepartureTime(Date departureDate) {
		super();
		this.departureDate = departureDate;
	}

	public DepartureTime() {
		super();
	}

	public int getTimeId() {
		return timeId;
	}

	public void setTimeId(int timeId) {
		this.timeId = timeId;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	
	
}
