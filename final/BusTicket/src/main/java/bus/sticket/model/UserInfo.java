/*
*Training Fresher Academy
*author Oracle-PC
*date 25 thg 7, 2018
*/
package bus.sticket.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="USER_INFO")
public class UserInfo {
	
	public static final String ADMIN_ROLE = "admin";
	public static final String SELLER_ROLE = "seller";
	public static final String CUSTOMER_ROLE = "customer";
	
	@Transient
	private String username;
	
	@Transient
	private String password;
	
	@Transient
	private String role;
	
	@Id
	@Column(name="UserId")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="fullName")
	private String fullName;
	
	@Column(name="IdentifyCard")
	private String identifyCard;

	//	@DateTimeFormat(pattern = "dd/MM/yyyy")

	@Column(name="BirthDay")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date birthDay;
	
	@Column(name="Email")
	private String email;
	
	@Column(name="PhoneNumber")
	private String phone;
	
	@Column(name="Gender")
	private byte gender;

	@Column(name="Address")
	private String address;
	
	
	
	public boolean isNull() {
		if(this.username!=""&&this.password!=""&&this.fullName!=""&&this.email!=""&&this.phone!=""&&this.identifyCard!=""&&this.address!="")
			return false;
		else 
			return true;
	}

	public UserInfo(String fullName, String identifyCard, Date birthDay, String email, String phone, byte gender,
			String address) {
		super();
		this.fullName = fullName;
		this.identifyCard = identifyCard;
		this.birthDay = birthDay;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.address = address;
	}

	public UserInfo(String username, String password, String fullName, String identifyCard, Date birthDay, String email,
			String phone, byte gender, String address) {
		super();
		this.username = username;
		this.password = password;
		this.fullName = fullName;
		this.identifyCard = identifyCard;
		this.birthDay = birthDay;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.address = address;
	}
	
	public UserInfo(String username, String password, String role, String fullName, String identifyCard, Date birthDay,
			String email, String phone, byte gender, String address) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
		this.fullName = fullName;
		this.identifyCard = identifyCard;
		this.birthDay = birthDay;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.address = address;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserInfo() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Date birthDay) {
		this.birthDay = birthDay;
	}

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public byte getGender() {
		return gender;
	}


	public void setGender(byte gender) {
		this.gender = gender;
	}


	public String getIdentifyCard() {
		return identifyCard;
	}


	public void setIdentifyCard(String identifyCard) {
		this.identifyCard = identifyCard;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
