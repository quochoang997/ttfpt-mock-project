/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TRIP")
public class Trip {
	@Id
	@Column(name="TripId")
	private int tripId;
	
	@Column(name="TripName")
	private String tripName;
	
	@Column(name="Cost")
	private float cost;
	
	@Column(name="TripLenght")
	private float tripLenght;
	
	@Column(name="BusType")
	private String busType;
	
	@Column(name="UserId")
	private int userId;
	
	@Column(name="Counts")
	private byte counts;

	public Trip(int tripId, String tripName, float cost, float tripLenght, String busType, int userId, byte counts) {
		super();
		this.tripId = tripId;
		this.tripName = tripName;
		this.cost = cost;
		this.tripLenght = tripLenght;
		this.busType = busType;
		this.userId = userId;
		this.counts = counts;
	}

	public Trip(String tripName, float cost, float tripLenght, String busType, int userId, byte counts) {
		super();
		this.tripName = tripName;
		this.cost = cost;
		this.tripLenght = tripLenght;
		this.busType = busType;
		this.userId = userId;
		this.counts = counts;
	}

	public Trip() {
		super();
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public String getTripName() {
		return tripName;
	}

	public void setTripName(String tripName) {
		this.tripName = tripName;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public float getTripLenght() {
		return tripLenght;
	}

	public void setTripLenght(float tripLenght) {
		this.tripLenght = tripLenght;
	}

	public String getBusType() {
		return busType;
	}

	public void setBusType(String busType) {
		this.busType = busType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public byte getCounts() {
		return counts;
	}

	public void setCounts(byte counts) {
		this.counts = counts;
	}

	@Override
	public String toString() {
		return tripId + tripName + tripLenght + cost + busType + userId + counts;
	}
	
}
