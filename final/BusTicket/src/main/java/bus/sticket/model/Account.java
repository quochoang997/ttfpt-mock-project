/*
*Training Fresher Academy
*author Oracle-PC
*date 30 thg 7, 2018
*/
package bus.sticket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ACCOUNT")
public class Account {
	@Id
	@Column(name="Username")
	private String username;
	
	@Column(name="Password")
	private String password;
	
	@Column(name="Role")
	private String role;

	@Column(name="UserId")
	private int userId;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Account(String username, String password, String role, int userId) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
		this.userId = userId;
	}

	public Account() {
		super();
	}
	
	public boolean isCustomer() {
		return this.role.equals(UserInfo.CUSTOMER_ROLE);
	}
	
	public boolean isSeller() {
		return this.role.equals(UserInfo.SELLER_ROLE);
	}
	
	public boolean isAdmin() {
		return this.role.equals(UserInfo.ADMIN_ROLE);
	}
}
