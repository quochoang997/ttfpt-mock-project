/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="STICKETS")
public class Stickets {
	
	@Id
	@Column(name="SticketId")
	private int sticketId;
	
	@Column(name="SeatNo")
	private int seatNo;

	public Stickets(int sticketId, int seatNo) {
		super();
		this.sticketId = sticketId;
		this.seatNo = seatNo;
	}

	public Stickets(int seatNo) {
		super();
		this.seatNo = seatNo;
	}

	public Stickets() {
		super();
	}

	public int getSticketId() {
		return sticketId;
	}

	public void setSticketId(int sticketId) {
		this.sticketId = sticketId;
	}

	public int getSeatNo() {
		return seatNo;
	}

	public void setSeatNo(int seatNo) {
		this.seatNo = seatNo;
	}
	
	
}
