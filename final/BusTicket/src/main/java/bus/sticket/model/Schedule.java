/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.model;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SCHEDULE")
public class Schedule {
	
	@Id
	@Column(name="ScheduleId")
	private int scheduleId;
	
	@Column(name="StartTime")
	private Time startTime;
	
	@Column(name="EndTime")
	private Time endTime;
	
	@Column(name="TripId")
	private int tripId;

	public Schedule(int scheduleId, Time startTime, Time endTime, int tripId) {
		super();
		this.scheduleId = scheduleId;
		this.startTime = startTime;
		this.endTime = endTime;
		this.tripId = tripId;
	}

	public Schedule(Time startTime, Time endTime, int tripId) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.tripId = tripId;
	}

	public Schedule() {
		super();
	}

	public int getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(int scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public Time getEndTime() {
		return endTime;
	}

	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}
	
	
}
