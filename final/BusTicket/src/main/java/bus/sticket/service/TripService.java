/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.service;

import java.util.List;

import bus.sticket.model.Schedule;
import bus.sticket.model.Trip;

public interface TripService {
	List<Trip> getAllTrip();
	Trip getTrip(int tripId);
	List<Schedule> getAllSchedule();
	List<Schedule> getListSchedulebyTripId(Trip trip);
}
