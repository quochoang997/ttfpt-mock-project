/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bus.sticket.dao.ScheduleDAO;
import bus.sticket.dao.TripDAO;
import bus.sticket.model.Schedule;
import bus.sticket.model.Trip;

@Service
public class TripServiceImpl implements TripService {
	
	@Autowired
	TripDAO tripDao;
	
	@Autowired
	ScheduleDAO scheduleDAO;
	
	public TripServiceImpl() {
		super();
	}

	public TripServiceImpl(TripDAO tripDao, ScheduleDAO scheduleDAO) {
		super();
		this.tripDao = tripDao;
		this.scheduleDAO = scheduleDAO;
	}

	public ScheduleDAO getScheduleDAO() {
		return scheduleDAO;
	}

	public void setScheduleDAO(ScheduleDAO scheduleDAO) {
		this.scheduleDAO = scheduleDAO;
	}

	public TripDAO getTripDao() {
		return tripDao;
	}

	public void setTripDao(TripDAO tripDao) {
		this.tripDao = tripDao;
	}

	public List<Trip> getAllTrip() {
		return tripDao.getAllTrip();
	}

	public Trip getTrip(int tripId) {
		return tripDao.getTrip(tripId);
	}

	public List<Schedule> getAllSchedule() {
		return scheduleDAO.getAllSchedule();
	}

	public List<Schedule> getListSchedulebyTripId(Trip trip) {
		return scheduleDAO.getListScheduleByScheduleId(trip.getTripId());
	}
	
}
