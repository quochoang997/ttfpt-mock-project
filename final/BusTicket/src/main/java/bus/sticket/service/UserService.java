/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 30, 2018 
 * @version 1.0
*/

package bus.sticket.service;

import bus.sticket.model.Account;
import bus.sticket.model.UserInfo;

public interface UserService {
	public Account login(UserInfo userInfo);
	public boolean register(UserInfo userInfo) ;
	public boolean updateInfo(UserInfo userInfo);
}
