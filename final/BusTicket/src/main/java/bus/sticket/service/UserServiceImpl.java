/*
*Training Fresher Academy
*author Oracle-PC
*date 27 thg 7, 2018
*/
package bus.sticket.service;

import java.util.List;

import javax.persistence.PersistenceException;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bus.sticket.dao.UserImplDAO;
import bus.sticket.model.Account;
import bus.sticket.model.UserInfo;

@Service
public class UserServiceImpl implements UserService{

	
	@Autowired
	private UserImplDAO userImplDAO;

	public Account login(UserInfo userInfo) {
		Session session = userImplDAO.getSessionFactory().openSession();
		session.beginTransaction();
		String hql = "FROM Account WHERE username like :username AND password like :password ";
		Query query = session.createQuery(hql);
		query.setParameter("username", userInfo.getUsername());
		query.setParameter("password", userInfo.getPassword());
		List<Account> results = (List<Account>) query.list();
		if (results.size() == 0)
			return null;
		else
			return results.get(0);
	}

	public boolean register(UserInfo userInfo) {
		String hql1 = "FROM UserInfo ORDER BY id DESC";
		Session session = userImplDAO.getSessionFactory().openSession();
		session.beginTransaction();
		
		try {
			session.save(userInfo);
			Query query = session.createQuery(hql1);
			List<UserInfo> results = (List<UserInfo>) query.list();
			session.save(new Account(userInfo.getUsername(),userInfo.getPassword(),userInfo.getRole()!=null?userInfo.getRole():"customer",results.get(0).getId()));
			session.getTransaction().commit();
		} catch (PersistenceException e) {
			System.out.println(e.toString());
			session.close();
			return false;
		}
		session.close();
		return true;
	}

	public boolean updateInfo(UserInfo userInfo) {
		Session session = userImplDAO.getSessionFactory().openSession();
		session.beginTransaction();
		String hql = "update UserInfo set identifyCard = :identifyCard,fullName = :fullName,email = :email,phone = :phone,gender = :gender,address = :address  where id like :id";
		Query query = session.createQuery(hql);
		query.setParameter("identifyCard", userInfo.getIdentifyCard());
		query.setParameter("fullName", userInfo.getFullName());
		query.setParameter("email", userInfo.getEmail());
		query.setParameter("phone", userInfo.getPhone());
		query.setParameter("gender", userInfo.getGender());
		query.setParameter("address", userInfo.getAddress());
		query.setParameter("id", userInfo.getId());
		query.executeUpdate();
		System.out.println(userInfo.getUsername() + "|" + userInfo.getFullName() + "|" + userInfo.getEmail() + "|"
				+ userInfo.getPhone() + "|"  + "|" + userInfo.getGender()+ "|" + userInfo.getId());
		return true;
	}

	
	

}
