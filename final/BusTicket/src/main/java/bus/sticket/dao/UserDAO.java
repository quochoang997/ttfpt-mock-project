/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 30, 2018 
 * @version 1.0
*/

package bus.sticket.dao;

import java.util.List;

import bus.sticket.model.UserInfo;


public interface UserDAO {
	UserInfo getUser(int id);
	List getUserByUserName();
	List getAllUser();
}
