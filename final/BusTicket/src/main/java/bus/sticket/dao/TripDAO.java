/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.dao;

import java.util.List;

import bus.sticket.model.Trip;

public interface TripDAO {
	List<Trip> getAllTrip();
	Trip getTrip(int tripId);
}
