/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import bus.sticket.model.Trip;

@Repository
public class TripImlpDAO implements TripDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public TripImlpDAO() {
		super();
	}

	public TripImlpDAO(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	public List<Trip> getAllTrip() {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		List<Trip> list = null;
		try {
			transaction = session.beginTransaction();
			list = session.createQuery("FROM bus.sticket.model.Trip").list();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		session.close();
		
		return list;
	}

	public Trip getTrip(int tripId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		Trip trip = null;
		try {
			transaction = session.beginTransaction();
			trip = session.get(Trip.class, tripId);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		session.close();
		
		return trip;
	}

}
