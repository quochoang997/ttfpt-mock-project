/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 30, 2018 
 * @version 1.0
*/

package bus.sticket.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import bus.sticket.model.UserInfo;

@Repository
public class UserImplDAO implements UserDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public UserInfo getUser(int id) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		String hql = "FROM UserInfo WHERE id like :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		List<UserInfo> results = (List<UserInfo>) query.list();
		if (results.size() == 0)
			return null;
		else
			return results.get(0);
	}

	public List<UserInfo> getAllUser() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		String hql = "FROM UserInfo";
		Query query = session.createQuery(hql);
		List<UserInfo> results = (List<UserInfo>) query.list();
		if (results.size() == 0)
			return null;
		else
			return results;
	}

	public List getUserByUserName() {
		return null;
	}
	
}
