/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import bus.sticket.model.Schedule;

@Repository
public class ScheduleImplDAO implements ScheduleDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public ScheduleImplDAO() {
		super();
	}

	public ScheduleImplDAO(SessionFactory sessionFactory) {
		super();
		this.sessionFactory = sessionFactory;
	}

	@SuppressWarnings("unchecked")
	public List<Schedule> getAllSchedule() {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		List<Schedule> list = null;
		try {
			transaction = session.beginTransaction();
			list = session.createQuery("FROM Schedule").list();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		session.close();
		
		return list;
	}

	public Schedule getSchedule(int scheduleId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		Schedule schedule = null;
		try {
			transaction = session.beginTransaction();
			schedule = session.get(Schedule.class, scheduleId);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		session.close();
		
		return schedule;
	}

	@SuppressWarnings("unchecked")
	public List<Schedule> getListScheduleByScheduleId(int tripId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		List<Schedule> list = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("FROM Schedule where tripId = :tripId");
			query.setParameter("tripId", tripId);
			list = query.getResultList();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}
		session.close();
		
		return list;
	}
	
}
