/*
 * My code is bestest =))
 * 
 * @author PhongDuong
 * Jul 31, 2018 
 * @version 1.0
*/

package bus.sticket.dao;

import java.util.List;

import bus.sticket.model.Schedule;

public interface ScheduleDAO {
	List<Schedule> getAllSchedule();
	Schedule getSchedule(int scheduleId);
	List<Schedule> getListScheduleByScheduleId(int tripId);
}
