
function openTab(curr, name) {
	
	var selectedButton = document.getElementsByClassName("selected_button")[0];
	selectedButton.className = selectedButton.className.replace(" selected_button", "");
	curr.className += " selected_button";

	/*show slide*/
	var slide = document.getElementsByClassName(name)[0];
	var allSlide = document.getElementsByClassName("slide");
	for (i = 0; i < allSlide.length; i++) {
		allSlide[i].style.display = "none";
	}
	slide.style.display = "block";
}

/*dialog global variable*/
var dialog = document.getElementById("login_dialog");

/*login event*/
function login(){
	dialog.style.display = "block";
}

window.onclick = function(event){
	if (event.target == dialog) {
        dialog.style.display = "none";
    }
}