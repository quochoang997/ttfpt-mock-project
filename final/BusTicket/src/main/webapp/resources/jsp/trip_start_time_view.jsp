<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>

	<title>Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/home_page.css" />">
	<link rel="stylesheet"
		href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" />">


</head>
<body>

	<header>
		<div class="header_container">
			<div class="logo">
				<a class="logo_link"> <img class="logo_img" src="<c:url value="/resources/image/logo.png" />"></a>
			</div>
		</div>
	</header>
	<article class="content">


		<!-- schedule list -->
		<div class="schedule">
			<h2>Trip</h2>
			<div class="trip_item">
				<table class="table trip_table">
					<thead class="thead-light">
						<tr>
							<th>No</th>
							<th>Trip Name</th>
							<th>Bus Type</th>
							<th>Trip Lenght</th>
							<th>Start Time</th>
							<th>End Time</th>
							<th>Cost</th>
							<th></th>
						</tr>
					</thead>

						<%!int count = 1; %>
						<c:forEach var="scheduleItem" items="${listSchedule}">
							<tr>
								<td>
									<%=count %>
								</td>
								<td>
									${trip.tripName}
								</td>
								<td>
									${trip.busType}
								</td>
								<td>
									${trip.tripLenght}
								</td>
								<td>
									${scheduleItem.startTime}
								</td>
								<td>
									${scheduleItem.endTime}
								</td>
								<td>
									${trip.cost}
								</td>
								<td>
									<a href="booking_action?scheduleId=${scheduleItem.scheduleId}">Booking</a>
								</td>
							</tr>
							<%count = count + 1; %>
						</c:forEach>
						
					</tbody>
				</table>
			</div>
		</div>

	</article>
	
	<script type="text/javascript" src="<c:url value="/resources/js/trip.js" />" ></script>
</body>
</html>