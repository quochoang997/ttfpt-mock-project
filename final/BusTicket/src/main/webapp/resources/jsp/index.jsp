<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>

	<title>Home</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/home_page.css" />">
	<link rel="stylesheet"
		href="<c:url value="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" />">


</head>
<body>

	<header>
		<div class="header_container">
			<div class="logo">
				<a class="logo_link">
					<img class="logo_img" src="<c:url value="/resources/image/logo.png" />">
				</a>
			</div>
			<div class="option_container">
				<div class="option"><a href="login">Login</a></div>
				<div class="option"><a href="register">Register</a></div>
			</div>
		</div>
	</header>
	
	<div>
		<a href="view_trip_action">view Trip</a>
	</div>
	

</body>
</html>