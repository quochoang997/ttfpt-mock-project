<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Insert title here</title>
	<style type="text/css" >
		<%@ include file="/resources/css/login.css"%>
	</style>
</head>
<body>
	<div class="container">
		<h1>Wellcome Seller!!!</h1>
		<hr>
		<form:form action="changeInfo" method="POST" modelAttribute="account">
			<div class="clearfix">
				<form:button>View all tickets booked</form:button>
			</div>
		</form:form>
		<form:form action="booking" method="POST" modelAttribute="account">
			<div class="clearfix">
				<form:button>Cancel ticket booked</form:button>
			</div>
		</form:form>
		<form:form action="userViewTicket" method="POST"
			modelAttribute="account">
			<div class="clearfix">
				<form:button>View all of available seat</form:button>
			</div>
		</form:form>
		<form:form action="userCancelTicket" method="POST"
			modelAttribute="account">
			<div class="clearfix">
				<form:button>Search & filter tickets and seats</form:button>
			</div>
		</form:form>
		<form:form action="userViewTicket" method="POST"
			modelAttribute="account">
			<div class="clearfix">
				<form:button>Update tickets & seats status</form:button>
			</div>
		</form:form>
	</div>
</body>
</html>