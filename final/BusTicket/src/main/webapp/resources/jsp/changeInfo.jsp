<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
	<style type="text/css" >
		<%@ include file="/resources/css/login_register.css"%>
	</style>
</head>
<body>
	<div class="container">
		<div class="container_nav">
			<div class="logo">
				<span>Change Information</span>
			</div>
		</div>

		<!-- register ui -->
		<div class="slide login_slide">
			<h1 class="header_text">Change your information</h1>
			<div class="form">
				<form:form action="updateInfo" method="POST"
					modelAttribute="userInfo">

					<div class="form_item">
						<img src="<c:url value="/resources/image/id_img.png" />"
							class="user_pass_img">
						<form:input path="id" cssClass="input_content" placeholder="Email"
							value="${userInfo.id}" disabled="true" />
					</div>

					<div class="form_item">
						<img src="<c:url value="/resources/image/fullname_img.png" />"
							class="user_pass_img">
						<form:input path="fullName" cssClass="input_content"
							value="${userInfo.fullName}"></form:input>
					</div>
					<div class="form_item">
						<img
							src="<c:url value="/resources/image/identify_card_img.png" />"
							class="user_pass_img">
						<form:input path="identifyCard" cssClass="input_content"
							value="${userInfo.identifyCard}"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/gender_img.png" />"
							class="user_pass_img">
						<form:radiobutton cssClass="radio_gender" path="gender" value="1" />
						Male
						<form:radiobutton cssClass="radio_gender" path="gender" value="0" />
						Female
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/phone_img.png" />"
							class="user_pass_img">
						<form:input path="phone" cssClass="input_content"
							value="${userInfo.phone}"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/address_img.png" />"
							class="user_pass_img">
						<form:input path="address" cssClass="input_content"
							value="${userInfo.address}"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/mail_img.png" />"
							class="user_pass_img">
						<form:input path="email" cssClass="input_content"
							placeholder="Email"></form:input>
					</div>

					<c:if test="${isAdmin==true}">
						<div class="form_item">
							<img src="<c:url value="/resources/image/role_img.png" />"
								class="user_pass_img">
							<form:select path="role" name="role" cssClass="input_content" >
								<c:if test="${userInfo.role=='admin'}">
									<form:option value="admin">Admin</form:option>
									<form:option value="seller">Seller</form:option>
									<form:option value="customer">Customer</form:option>
								</c:if>
								<c:if test="${userInfo.role=='seller'}">
									<form:option value="seller">Seller</form:option>
									<form:option value="customer">Customer</form:option>
									<form:option value="admin">Admin</form:option>
								</c:if>
								<c:if test="${userInfo.role=='customer'}">
									<form:option value="customer">Customer</form:option>
									<form:option value="seller">Seller</form:option>
									<form:option value="admin">Admin</form:option>
								</c:if>
							</form:select>
						</div>
					</c:if>
					<button class="submit">Change</button>
				</form:form>
			</div>
		</div>
	</div>
	<script type="text/javascript"
		src="<c:url value="/resources/js/login_register.js" />"></script>

</body>
</html>

