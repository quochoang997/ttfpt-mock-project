<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
	<style type="text/css" >
		<%@ include file="/resources/css/login_register.css"%>
	</style>
</head>
<body>

	

	<div class="container">
		<div class="container_nav">
			<div class="logo"><span>Bus Sticket</span></div>
			<div class="menu">
				<button class="login_register selected_button" onclick="openTab(this, 'login_slide')">Login</button>
				<button class="login_register" onclick="openTab(this, 'register_slide')">Register</button>
			</div>

		</div>
		<!-- login ui -->
		<div class="slide login_slide">
			<h1 class="header_text">Welcome</h1>
			<div class="form">
				<form:form action="dashboard" modelAttribute="userInfo">
					<div class="form_item">
						<img src="<c:url value="/resources/image/user_login.png" />" class="user_pass_img">
						<form:input  path="username" cssClass="input_content" placeholder="Username"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/pass_login.png" />" class="user_pass_img">
						<form:input path="password" cssClass="input_content" placeholder="Password"></form:input>
					</div>
					<button class="submit">Enter</button>
				</form:form>
			</div>
		</div>

		<!-- register ui -->
		<div class="slide register_slide" >
			<h1 class="header_text">Register Account</h1>
			<div class="form">
				<form:form action="userRegister" method="POST" modelAttribute="userInfo" >
					<div class="form_item">
						<img src="<c:url value="/resources/image/user_login.png" />" class="user_pass_img">
						<form:input path="username" cssClass="input_content" placeholder="Username"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/pass_login.png" />" class="user_pass_img">
						<form:input path="password" cssClass="input_content" placeholder="Password"></form:input>
					</div>
					
					<div class="form_item">
						<img src="<c:url value="/resources/image/fullname_img.png" />" class="user_pass_img">
						<form:input path="fullName" cssClass="input_content" placeholder="Full Name"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/identify_card_img.png" />" class="user_pass_img">
						<form:input path="identifyCard" cssClass="input_content" placeholder="Identify Card"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/gender_img.png" />" class="user_pass_img">
						<form:radiobutton cssClass="radio_gender" path="gender" value="1" />Male
						<form:radiobutton cssClass="radio_gender" path="gender" value="0" />Female
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/birthday_img.png" />" class="user_pass_img">
						<%-- <form:input path="birthDay" cssClass="input_content" placeholder="Birthday"></form:input> --%>
						<form:input path="birthDay" cssClass="input_content" placeholder="31/12/2000"></form:input>
						
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/phone_img.png" />" class="user_pass_img">
						<form:input path="phone" cssClass="input_content" placeholder="Phone Number"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/address_img.png" />" class="user_pass_img">
						<form:input path="address" cssClass="input_content" placeholder="Address"></form:input>
					</div>
					<div class="form_item">
						<img src="<c:url value="/resources/image/mail_img.png" />" class="user_pass_img">
						<form:input path="email" cssClass="input_content" placeholder="Email"></form:input>
					</div>
					<button class="submit">Register</button>
				</form:form>
			</div>
		</div>
	</div>


	<script type="text/javascript" src="<c:url value="/resources/js/login_register.js" />" ></script>
	
</body>
</html>
