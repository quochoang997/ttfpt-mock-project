<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}
div{
	overflow-x:auto;
}
tr:nth-child(even){background-color: #f2f2f2}
</style>
</head>
<body>
	<%int size = 1; %>
	<h2>View All User</h2>

	<c:if test="${not empty listUser}">
	<div>
		<table>
			<tr>
		      <th>No</th>
		      <th>Full Name</th>
		      <th>Identify Card</th>
		      <th>BirthDay</th>
		      <th>Email</th>
		      <th>PhoneNumber</th>
		      <th>Gender</th>
		      <th>Address</th>
		      <th>Action</th>
		    </tr>
			<c:forEach var="listValue" items="${listUser}">
				<tr>
					
					<td><%=size%></td>
					<td>${listValue.fullName}</td>
					<td>${listValue.identifyCard}</td>	
					<td>${listValue.birthDay}</td>	
					<td>${listValue.email}</td>	
					<td>${listValue.phone}</td>	
					<td><c:if test="${listValue.gender==1}">Male</c:if>
						<c:if test="${listValue.gender==0}">Female</c:if>
					</td>	
					<td>${listValue.address}</td>	
					<td>
						<%-- <form:form action="deocogi" method="POST" modelAttribute="userInfooo">
							<button class="submit">Update Info</button>
						</form:form> --%>
						<%-- <c:set var="userInfoo" value="${listValue.fullName}"/> --%>
						<a href="deocogi?userInfoID=${listValue.id}">Update Info</a>
					</td>
				</tr>
				<%size++;%>
			</c:forEach>
		</table>
	</div>
	</c:if>
</body>
</html>