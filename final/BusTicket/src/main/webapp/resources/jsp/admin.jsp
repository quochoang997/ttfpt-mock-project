<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Insert title here</title>
	<style type="text/css" >
		<%@ include file="/resources/css/login.css"%>
	</style>
</head>
<body>
	<div class="container">
		<h1>Wellcome Admin!!!</h1>
		<hr>
		<form:form action="adCreateUser" method="POST" modelAttribute="userInfo">
			<div class="clearfix">
				<form:button >Create new user</form:button>
			</div>
		</form:form>
				<form:form action="booking" method="POST" modelAttribute="userInfo">
			<div class="clearfix">
				<form:button >Set Role</form:button>
			</div>
		</form:form>
				<form:form action="viewAllUser" method="POST" modelAttribute="userInfo">
			<div class="clearfix">
				<form:button >View all of user</form:button>
			</div>
		</form:form>
				<form:form action="userCancelTicket" method="POST" modelAttribute="userInfo">
			<div class="clearfix">
				<form:button >Cancel ticket booked</form:button>
			</div>
		</form:form>
	</div>
</body>
</html>